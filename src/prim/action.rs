use super::buffer::Size;

pub enum Action {
    Updated(Size),
    Cursor(Size),
}
