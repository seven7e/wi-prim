use super::buffer::{RBuffer, RWBuffer, RCursor, Cursor};

pub struct State {
    buffer: Box<dyn RWBuffer>,
    cursor: Box<dyn Cursor>,
}

pub struct RState {
    buffer: Box<dyn RBuffer>,
    cursor: Box<dyn RCursor>,
}

// impl State {
//     fn ro(&self) -> RState {
//         RState {
//             buffer: Box::new(*self.buffer),
//             cursor: self.cursor,
//         }
//     }
// }
