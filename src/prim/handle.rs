use super::event::Event;
use super::state::State;

pub trait Handler {
    fn handle(&self, event: Event, state: State) -> State;
}
