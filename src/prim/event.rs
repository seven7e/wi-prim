// pub trait Event {
//     fn event(&self) -> String;
// }

use super::buffer::{Size, Elem};

pub enum Event {
    MoveTo(Size),
    Insert(Elem),
    Delete,
}
