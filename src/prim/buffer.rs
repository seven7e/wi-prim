pub type Elem = char;
pub type Size = usize;

pub trait RBuffer {
    // fn get_cursor_pos(&self) -> Size;
    // fn move_cursor(&self, pos: Size);
    fn length(&self) -> Size;
    fn get_elem(&self, pos: Size) -> Elem;
}

pub trait WBuffer {
    fn set_elem(&mut self, pos: Size, value: Elem);
}

pub trait RWBuffer: RBuffer + WBuffer {}

pub trait RCursor {
    fn get(&self) -> Size;
}

pub trait WCursor {
    fn move_to(&mut self, pos: Size);
}

pub trait Cursor: RCursor + WCursor {}
