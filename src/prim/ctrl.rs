use super::action::Action;
use super::state::State;

pub trait Controller {
    fn control(&self, state: &State) -> Action;
}
