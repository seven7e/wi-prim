pub(crate) type Tags = Vec<String>;

pub trait Tagger {
    fn tag(&self) -> Tags;
}
